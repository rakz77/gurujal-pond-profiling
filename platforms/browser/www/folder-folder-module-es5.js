function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["folder-folder-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/folder/folder.page.html":
  /*!*******************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/folder/folder.page.html ***!
    \*******************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppFolderFolderPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>{{ folder }}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\">\n  <ion-header collapse=\"condense\">\n    <ion-toolbar>\n      <ion-title size=\"large\">{{ folder }}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <div id=\"container\" *ngIf=\"folder== 'Home'\">\n    <div id=\"map\" style=\"width: 100%; height: 60%;\"></div>\n    \n    <img style=\"text-align: center; height: 100%; vertical-align: middle;\" src='/assets/img/logo-gurujal.png'/>\n    \n  </div>\n\n  <div id=\"container\" *ngIf=\"folder== 'Ponds'\">\n    <div class=\"ion-padding\">\n      <ion-button (click)=\"openModal()\">Add New Record</ion-button>\n    </div>\n    <div>\n      <ion-label style=\"padding-left: 21px;\"> Search by block :</ion-label>\n      <ion-card>\n        <ion-select placeholder=\"Select\" [(ngModel)]=\"block\" (ionChange)=\"getPondDataByBlock(block)\">\n          <ion-select-option value=\"Farukhnagar\">Farukhnagar</ion-select-option>\n          <ion-select-option value=\"Gurgaon\">Gurugram</ion-select-option>\n          <ion-select-option value=\"Pataudi\">Pataudi</ion-select-option>\n          <ion-select-option value=\"Sohna\">Sohna</ion-select-option>\n        </ion-select>\n      </ion-card>\n    </div>\n    <div>\n      <ion-list *ngFor=\"let data of pondDataByBlock\">\n      <ion-card>\n        <ion-card-header style=\"text-align: center;\">\n          <ion-card-subtitle>{{ data.tehsilName }}</ion-card-subtitle>\n          <ion-card-title>{{ data.title }}</ion-card-title>\n        </ion-card-header>\n      \n        <ion-card-content style=\"text-align: center;\">\n            \n          <!-- <img [src]=\"data.image\" style=\"height: 150px; width: 150px\" /><br> -->\n            Area : {{ data.area}}<br>\n            Latitude : {{ data.latitude}}<br>\n            Longitude : {{ data.longitude }}\n        </ion-card-content>\n        <ion-button (click)=\"openDetailsModal(data);\" expand=\"block\" style=\"padding-left: 12px; padding-right: 12px; height: 20px; font-size: 10px;\">Click for more details</ion-button>\n      </ion-card>\n      </ion-list>\n    </div>\n  </div>\n  <div id=\"container\" *ngIf=\"folder== 'Checklist'\" class=\"ionstyle\">\n    <ion-card>\n      <table>\n        <tr>\n          <td>\n            <ion-label position=\"stacked\">\n              &nbsp;&nbsp;&nbsp;your country\n            </ion-label>\n            <ion-select placeholder=\"Select\" style=\"padding-left: 26px;\">\n              <ion-select-option value=\"Farukhnagar\">Farukhnagar</ion-select-option>\n              <ion-select-option value=\"Gurgaon\">Gurugram</ion-select-option>\n              <ion-select-option value=\"Pataudi\">Pataudi</ion-select-option>\n              <ion-select-option value=\"Sohna\">Sohna</ion-select-option>\n            </ion-select>\n          </td>\n        </tr>\n    \n        <tr>\n          <td>\n            <ion-label position=\"stacked\">\n              &nbsp;&nbsp;&nbsp;mobile\n            </ion-label>\n            <ion-input name=\"phone\" placeholder=\"Enter phone number\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </ion-input>\n          </td>\n        </tr>\n    \n        <tr>\n          <td>\n            <ion-label position=\"stacked\">\n              &nbsp;&nbsp;&nbsp;code\n            </ion-label>\n            <ion-input name=\"customer\" placeholder=\"Enter user code\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</ion-input>\n          </td>\n        </tr>\n    \n        <tr>\n          <td>\n      <ion-button expand=\"block\" >Submit</ion-button>\n          </td>\n        </tr>\n        </table>\n  </ion-card>\n  </div>\n\n  <div id=\"container\" *ngIf=\"folder== 'Gallery'\">\n    <!-- <iframe src=\"https:gurujal.org/imageupload\" style=\"height: 100%;; width: 100%;\"></iframe> -->\n    <ion-item>\n    <ion-select [(ngModel)]=\"pond.title\" placeholder=\"Select Pond\" (ionChange)=\"onChange(pond.title)\">\n      <ion-select-option *ngFor=\"let pond of pondData\" [value]=\"pond.title\">{{pond.title}} \n      </ion-select-option>\n    </ion-select>\n  </ion-item>\n  <ion-card [hidden]=ishidden>\n    <img [src]=\"image\" style=\"height: 200px; width: 200px\" />\n  </ion-card>\n  <br>\n\n    <input [hidden]=ishidden type=\"file\" accept=\"image/*\" (change)=\"uploadFile($event)\"/>\n    <br>\n    <div class=\"progress\" [hidden]=ishidden>\n      <div class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" [style.width]= \"(uploadProgress | async) + '%'\" [attr.aria-valuenow]= \"(uploadProgress | async)\" aria-valuemin=\"0\" aria-valuemax=\"100\">\n    </div>\n    </div>\n    \n      <div [hidden]=ishidden style=\"font-size: 10px;\">Upload progress : {{ uploadProgress | async }} % </div>\n      <a [hidden]=ishidden [href]=\"downloadURL | async\">{{ downloadURL | async }}</a>\n      <ion-button [hidden]=ishidden expand=\"block\" (click)=\"updatePondImage(pondImage, url1);\">Upload and change image</ion-button>\n    \n    </div>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/folder/folder-routing.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/folder/folder-routing.module.ts ***!
    \*************************************************/

  /*! exports provided: FolderPageRoutingModule */

  /***/
  function srcAppFolderFolderRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FolderPageRoutingModule", function () {
      return FolderPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _folder_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./folder.page */
    "./src/app/folder/folder.page.ts");

    var routes = [{
      path: '',
      component: _folder_page__WEBPACK_IMPORTED_MODULE_3__["FolderPage"]
    }];

    var FolderPageRoutingModule = function FolderPageRoutingModule() {
      _classCallCheck(this, FolderPageRoutingModule);
    };

    FolderPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], FolderPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/folder/folder.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/folder/folder.module.ts ***!
    \*****************************************/

  /*! exports provided: FolderPageModule */

  /***/
  function srcAppFolderFolderModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FolderPageModule", function () {
      return FolderPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _folder_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./folder-routing.module */
    "./src/app/folder/folder-routing.module.ts");
    /* harmony import */


    var _folder_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./folder.page */
    "./src/app/folder/folder.page.ts");

    var FolderPageModule = function FolderPageModule() {
      _classCallCheck(this, FolderPageModule);
    };

    FolderPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _folder_routing_module__WEBPACK_IMPORTED_MODULE_5__["FolderPageRoutingModule"]],
      declarations: [_folder_page__WEBPACK_IMPORTED_MODULE_6__["FolderPage"]]
    })], FolderPageModule);
    /***/
  },

  /***/
  "./src/app/folder/folder.page.scss":
  /*!*****************************************!*\
    !*** ./src/app/folder/folder.page.scss ***!
    \*****************************************/

  /*! exports provided: default */

  /***/
  function srcAppFolderFolderPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-menu-button {\n  color: var(--ion-color-primary);\n}\n\n#container {\n  left: 0;\n  right: 0;\n  top: 50%;\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n#container a {\n  text-decoration: none;\n}\n\nion-content ion-toolbar {\n  --background: transparent;\n}\n\n.ionstyle td ion-input {\n  position: relative;\n  width: auto;\n}\n\n.ionstyle td ion-card {\n  background-color: #d3d3d3;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYWtlc2hnb2dvaS9EZXNrdG9wL3BvbmRQcm9maWxpbmcvc3JjL2FwcC9mb2xkZXIvZm9sZGVyLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZm9sZGVyL2ZvbGRlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrQkFBQTtBQ0NGOztBREVBO0VBQ0UsT0FBQTtFQUNBLFFBQUE7RUFDQSxRQUFBO0FDQ0Y7O0FERUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUNDRjs7QURFQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0FDQ0Y7O0FERUE7RUFDRSxxQkFBQTtBQ0NGOztBREVBO0VBQ0UseUJBQUE7QUNDRjs7QURHRTtFQUNFLGtCQUFBO0VBQ0MsV0FBQTtBQ0FMOztBREVFO0VBQ0UseUJBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL2ZvbGRlci9mb2xkZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLW1lbnUtYnV0dG9uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cblxuI2NvbnRhaW5lciB7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDUwJTtcbn1cblxuI2NvbnRhaW5lciBzdHJvbmcge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGxpbmUtaGVpZ2h0OiAyNnB4O1xufVxuXG4jY29udGFpbmVyIHAge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICBjb2xvcjogIzhjOGM4YztcbiAgbWFyZ2luOiAwO1xufVxuXG4jY29udGFpbmVyIGEge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbmlvbi1jb250ZW50IGlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuLmlvbnN0eWxlIHRkIHtcbiAgaW9uLWlucHV0e1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgd2lkdGg6YXV0bztcbiAgfVxuICBpb24tY2FyZCB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojZDNkM2QzOyAvLyBibGFja1xuICB9XG59XG4iLCJpb24tbWVudS1idXR0b24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xufVxuXG4jY29udGFpbmVyIHtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogNTAlO1xufVxuXG4jY29udGFpbmVyIHN0cm9uZyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbGluZS1oZWlnaHQ6IDI2cHg7XG59XG5cbiNjb250YWluZXIgcCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDIycHg7XG4gIGNvbG9yOiAjOGM4YzhjO1xuICBtYXJnaW46IDA7XG59XG5cbiNjb250YWluZXIgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuaW9uLWNvbnRlbnQgaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4uaW9uc3R5bGUgdGQgaW9uLWlucHV0IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogYXV0bztcbn1cbi5pb25zdHlsZSB0ZCBpb24tY2FyZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2QzZDM7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/folder/folder.page.ts":
  /*!***************************************!*\
    !*** ./src/app/folder/folder.page.ts ***!
    \***************************************/

  /*! exports provided: FolderPage */

  /***/
  function srcAppFolderFolderPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FolderPage", function () {
      return FolderPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./../api.service */
    "./src/app/api.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _create_pond_modal_create_pond_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../create-pond-modal/create-pond-modal.page */
    "./src/app/create-pond-modal/create-pond-modal.page.ts");
    /* harmony import */


    var _pond_details_pond_details_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../pond-details/pond-details.page */
    "./src/app/pond-details/pond-details.page.ts");
    /* harmony import */


    var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/in-app-browser/ngx */
    "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
    /* harmony import */


    var angularfire2_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! angularfire2/storage */
    "./node_modules/angularfire2/storage/index.js");
    /* harmony import */


    var angularfire2_storage__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(angularfire2_storage__WEBPACK_IMPORTED_MODULE_9__);
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var FolderPage = /*#__PURE__*/function () {
      function FolderPage(activatedRoute, api, modalContoller, iab, http, storage) {
        _classCallCheck(this, FolderPage);

        this.activatedRoute = activatedRoute;
        this.api = api;
        this.modalContoller = modalContoller;
        this.iab = iab;
        this.http = http;
        this.storage = storage;
        this.ishidden = true;
        this.latitude = "";
        this.longitude = "";
        this.timestamp = "";
        this.selectedFile = null;
        this.image = null;
        this.url1 = '';
        this.pond = {
          id: "",
          title: "",
          imageA: "",
          imageGps: ""
        };
        this.checklistDataFromService = {
          title: '',
          location: '',
          latitude: '',
          longitude: '',
          area: '',
          meanDepth: '',
          waterBodyType: '',
          currentStatus: '',
          sourceOfWater: '',
          outflowDescription: '',
          waterLevelChanges: '',
          waterFlowWithin5KmRadius: '',
          groundwaterLevel: '',
          waterDryOut: '',
          catchmentArea: '',
          landUseOfCatchmentArea: '',
          totalPopulation: '',
          waterBodyUsedByAnimals: '',
          solidWasteDumping: '',
          solidWasteDisposal: '',
          sourceOfPollution: '',
          nutrientLevel: '',
          floraAndFauna: '',
          waterUsedFor: '',
          waterBodyFunction: '',
          majorProblems: '',
          constructionActivities: '',
          awarenessOfLocalCommunities: '',
          interestOfLocalCommunities: '',
          ngOsInterested: '',
          requiredRestorationActivities: '',
          physicalDescription: '',
          geotaggedPictures: '',
          ownership: '',
          khasraNumber: '',
          landscaping: '',
          freeSpace: '',
          block: ''
        };
      }

      _createClass(FolderPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.folder = this.activatedRoute.snapshot.paramMap.get('id');
          this.getPond();
        }
      }, {
        key: "openModal",
        value: function openModal() {
          this.modalContoller.create({
            component: _create_pond_modal_create_pond_modal_page__WEBPACK_IMPORTED_MODULE_6__["CreatePondModalPage"]
          }).then(function (modalElement) {
            modalElement.present();
          });
        }
      }, {
        key: "openDetailsModal",
        value: function openDetailsModal(request) {
          var req = request;
          this.modalContoller.create({
            component: _pond_details_pond_details_page__WEBPACK_IMPORTED_MODULE_7__["PondDetailsPage"],
            componentProps: {
              title: req.title,
              area: req.area,
              latitude: req.latitude,
              longitude: req.longitude,
              uniqueId: req.uniqueId,
              capacity: req.capacity,
              ownership: req.ownership,
              tehsilName: req.tehsilName,
              villageName: req.villageName,
              image: req.image
            }
          }).then(function (modalElement) {
            modalElement.present();
          });
        }
      }, {
        key: "getPond",
        value: function getPond() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var _this = this;

            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.api.getPondData().subscribe(function (res) {
                      // console.log(res.items);
                      _this.pondData = res.items; // console.log(this.pondData);
                    }, function (err) {// console.log(err);
                    });

                  case 2:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "getPondDataByBlock",
        value: function getPondDataByBlock(request) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this2 = this;

            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.api.getPondDataByBlock(request).subscribe(function (res) {
                      // console.log(res.items);
                      _this2.pondDataByBlock = res.items; // console.log(this.pondDataByBlock);
                    }, function (err) {// console.log(err);
                    });

                  case 2:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "saveChecklist",
        value: function saveChecklist(data) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var dataToSend;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    dataToSend = {
                      title: data.title,
                      location: data.tehsilName,
                      latitude: data.area,
                      longitude: data.longitude,
                      area: data.area,
                      meanDepth: data.meanDepth,
                      waterBodyType: data.waterBodyType,
                      currentStatus: data.currentStatus,
                      sourceOfWater: data.sourceOfWater,
                      outflowDescription: data.outflowDescription,
                      waterLevelChanges: data.waterLevelChanges,
                      waterFlowWithin5KmRadius: data.waterFlowWithin5KmRadius,
                      groundwaterLevel: data.groundwaterLevel,
                      waterDryOut: data.waterDryOut,
                      catchmentArea: data.catchmentArea,
                      landUseOfCatchmentArea: data.landUseOfCatchmentArea,
                      totalPopulation: data.totalPopulation,
                      waterBodyUsedByAnimals: data.waterBodyUsedByAnimals,
                      solidWasteDumping: data.solidWasteDumping,
                      solidWasteDisposal: data.solidWasteDisposal,
                      sourceOfPollution: data.sourceOfPollution,
                      nutrientLevel: data.nutrientLevel,
                      floraAndFauna: data.floraAndFauna,
                      waterUsedFor: data.waterUsedFor,
                      waterBodyFunction: data.waterBodyFunction,
                      majorProblems: data.majorProblems,
                      constructionActivities: data.constructionActivities,
                      awarenessOfLocalCommunities: data.awarenessOfLocalCommunities,
                      interestOfLocalCommunities: data.interestOfLocalCommunities,
                      ngOsInterested: data.ngOsInterested,
                      requiredRestorationActivities: data.requiredRestorationActivities,
                      physicalDescription: data.physicalDescription,
                      geotaggedPictures: data.geotaggedPictures,
                      ownership: data.ownership,
                      khasraNumber: data.khasraNumber,
                      landscaping: data.landscaping,
                      freeSpace: data.freeSpace,
                      block: data.block
                    };
                    this.api.postPondChecklist(dataToSend).subscribe(function (res) {// console.log(res.items);
                    }, function (err) {// console.log(err);
                    });

                  case 2:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "updatePondImage",
        value: function updatePondImage(data, loc) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var dataToSend;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    dataToSend = {
                      imageA: loc,
                      _id: data._id,
                      title: data.title
                    };
                    this.api.putPondImage(dataToSend);

                  case 2:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "uploadFile",
        value: function uploadFile(event) {
          var _this3 = this;

          this.file = event.target.files[0];

          if (this.file) {
            var filePath = Math.random().toString(13).substring(2) + this.file.name;
            var fileRef = this.storage.ref(filePath);
            var task = this.storage.upload(filePath, this.file); // observe percentage changes

            this.uploadProgress = task.percentageChanges(); // get notified when the download URL is available

            task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["finalize"])(function () {
              _this3.downloadURL = fileRef.getDownloadURL(); // {{ downloadURL | async }}

              _this3.downloadURL.subscribe(function (url) {
                _this3.url1 = url;
              });
            })).subscribe();
          } else {
            console.log('Ooppsss');
          }
        }
      }, {
        key: "onChange",
        value: function onChange(request) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var _this4 = this;

            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.api.getPondImageByTitle(request).subscribe(function (res) {
                      _this4.pondImage = res.items[0];
                      _this4.image = _this4.pondImage.imageA;
                      _this4.ishidden = false;
                    }, function (err) {// console.log(err);
                    });

                  case 2:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }]);

      return FolderPage;
    }();

    FolderPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
      }, {
        type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_8__["InAppBrowser"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
      }, {
        type: angularfire2_storage__WEBPACK_IMPORTED_MODULE_9__["AngularFireStorage"]
      }];
    };

    FolderPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-folder',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./folder.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/folder/folder.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./folder.page.scss */
      "./src/app/folder/folder.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_8__["InAppBrowser"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], angularfire2_storage__WEBPACK_IMPORTED_MODULE_9__["AngularFireStorage"]])], FolderPage);
    /***/
  }
}]);
//# sourceMappingURL=folder-folder-module-es5.js.map