const functions = require("firebase-functions");
const cors = require("cors")({origin: true});
const path = require('path');
const os = require('os');
const Busboy = require("busboy");
const fs = require("fs");

const { Storage } = require('@google-cloud/storage');
const projectId = 'gurujal-b609e';
const keyFilename = 'gurujal-b609e-firebase-adminsdk-3ogid-be168e8e53.json';
let gcs = new Storage ({
  projectId, keyFilename
});


exports.uploadFile = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
      if (req.method !== "POST") {
        return res.status(500).json({
          message: "Not allowed"
        });
      }
      const busboy = new Busboy({ headers: req.headers });
      let uploadData = null;
  
      busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
        const filepath = path.join(os.tmpdir(), filename);
        uploadData = { file: filepath, type: mimetype };
        file.pipe(fs.createWriteStream(filepath));
      });
  
      busboy.on("finish", () => {
        const bucket = gcs.bucket("gurujal-b609e.appspot.com");
        bucket
          .upload(uploadData.file, {
            uploadType: "media",
            metadata: {
              metadata: {
                contentType: uploadData.type
              }
            }
          })
          .then(() => {
            res.status(200).json({
              message: "It worked!",
            });
            return;
          })
          .catch(err => {
            res.status(500).json({
              error: err
            });
          });
      });
      busboy.end(req.rawBody);
    });
  });