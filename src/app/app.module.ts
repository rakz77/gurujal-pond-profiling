import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CreatePondModalPageModule } from './create-pond-modal/create-pond-modal.module';
import { PondDetailsPageModule } from './pond-details/pond-details.module';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';  
import { HTTP } from '@ionic-native/http/ngx';
import { AngularFireStorageModule } from 'angularfire2/storage';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    CreatePondModalPageModule,
    PondDetailsPageModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    InAppBrowser
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
