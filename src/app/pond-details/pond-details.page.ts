import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from './../api.service';
import { NavParams } from '@ionic/angular';
import {HttpClient} from '@angular/common/http';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-pond-details',
  templateUrl: './pond-details.page.html',
  styleUrls: ['./pond-details.page.scss'],
})
export class PondDetailsPage implements OnInit {
    file;
    downloadURL: Observable<string>;
    ref: AngularFireStorageReference;
    task: AngularFireUploadTask;
    uploadProgress: Observable<number>;

  title = '';
  area = '';
  latitude = '';
  longitude = '';
  tehsilName = '';
  uniqueId = '';
  capacity = '';
  ownership = '';
  villageName = '';
  image = null;
  constructor(private modalController: ModalController, 
    public api: ApiService, 
    private navParams: NavParams,
    private http:HttpClient,
    private storage: AngularFireStorage) { }

  ngOnInit() {
    this.title = this.navParams.get('title');
    this.area = this.navParams.get('area');
    this.latitude = this.navParams.get('latitude');
    this.longitude = this.navParams.get('longitude');
    this.uniqueId = this.navParams.get('uniqueId');
    this.tehsilName = this.navParams.get('tehsilName');
    this.capacity = this.navParams.get('capacity');
    this.ownership = this.navParams.get('ownership');
    this.villageName = this.navParams.get('villageName');
    this.image = this.navParams.get('image');
  }
  
  closeDetailsModal() {
    this.modalController.dismiss();
  }

  async getSinglePondData(request) {
    await this.api.getPondDataByBlock(request)
        .subscribe(res => {
            // console.log(res.items);
            this.getSinglePondData = res.items;
            // console.log(this.pondDataByBlock);
        }, err => {
            // console.log(err);
    });   
}

uploadFile(event) {
  this.file = event.target.files[0];
  if (this.file) {
    const filePath = Math.random().toString(13).substring(2) + this.file.name;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, this.file);

    // observe percentage changes
    this.uploadProgress = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL(); // {{ downloadURL | async }}
          this.downloadURL.subscribe(url => {
          });
        })
      )
      .subscribe();
  } else {
    console.log('Ooppsss');
  }
}

}
