import { Injectable } from '@angular/core';
import { HTTP } from "@ionic-native/http/ngx";
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const apiUrl = "https://gurujal.org/_functions/pondList";
const apiUrl1 = "https://gurujal.org/_functions/pondListByBlock/";
const apiUrl2 = "https://gurujal.org/_functions/savePond";
const apiUrl3 = "https://gurujal.org/_functions/singlePond";
const apiUrl4 = "https://gurujal.org/_functions/saveChecklist";
const apiUrl5 = "https://gurujal.org/_functions/pondImageByTitle/";
const apiUrl6 = "https://gurujal.org/_functions/updatePondImage";


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient, private http1: HTTP) { }
  block = '';
  title = '';
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getPondData(): Observable<any> {
    return this.http.get(apiUrl).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getPondDataByBlock(request): Observable<any> {
    this.block = request;
    return this.http.get(apiUrl1+this.block).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getPondImageByTitle(request): Observable<any> {
    this.title = request;
    return this.http.get(apiUrl5+this.title).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getSinglePondData(request): Observable<any> {
    this.block = request;
    return this.http.get(apiUrl3+this.block).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  postPondData(data): Observable<any> {
    return this.http.post(apiUrl2,data).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  putPondImage(data): Observable<any> {
    return this.http.put(apiUrl6,data).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }


  
  postPondChecklist(data): Observable<any> {
    return this.http.post(apiUrl4,data).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }
} 
