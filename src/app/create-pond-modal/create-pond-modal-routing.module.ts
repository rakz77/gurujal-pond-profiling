import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreatePondModalPage } from './create-pond-modal.page';

const routes: Routes = [
  {
    path: '',
    component: CreatePondModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreatePondModalPageRoutingModule {}
