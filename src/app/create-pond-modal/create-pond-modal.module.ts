import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreatePondModalPageRoutingModule } from './create-pond-modal-routing.module';

import { CreatePondModalPage } from './create-pond-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreatePondModalPageRoutingModule
  ],
  declarations: [CreatePondModalPage]
})
export class CreatePondModalPageModule {}
