import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreatePondModalPage } from './create-pond-modal.page';

describe('CreatePondModalPage', () => {
  let component: CreatePondModalPage;
  let fixture: ComponentFixture<CreatePondModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePondModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreatePondModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
