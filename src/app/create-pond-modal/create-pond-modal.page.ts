import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ApiService} from './../api.service';

import {v4 as uuidv4} from 'uuid';

declare const window: any;
declare const require: any;

const aws = require('aws-sdk/dist/aws-sdk');


@Component({
    selector: 'app-create-pond-modal',
    templateUrl: './create-pond-modal.page.html',
    styleUrls: ['./create-pond-modal.page.scss'],
})
export class CreatePondModalPage implements OnInit {
    dataFromService: {
        title?: string;
        area?: string;
        tehsilName?: string;
        file: any;
    } = {
        title: '',
        area: '',
        tehsilName: '',
        file: null
    };
    files: any = [];

    constructor(private modalController: ModalController, public api: ApiService) {
    }

    ngOnInit() {
    }

    closeModal() {
        this.modalController.dismiss();
    }


    fileEvent(event) {
        this.files = event.target.files;
    }

    savePond(data) {
        const p = new Promise((resolve, reject) => {
            if (this.files && this.files.length) {
                const awsService = window.AWS;
                const fileName = uuidv4() + '.jpg';
                console.log(aws);
                console.log(awsService);
                awsService.config.accessKeyId = 'AKIASMPLK2WUCJMWNNLW';
                awsService.config.secretAccessKey = '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';
                const bucket = new awsService.S3({
                    params: {
                        Bucket: 'gurujal.org',
                        ACL: 'public-read'
                    }
                });
                const params = {Key: fileName, Body: this.files[0], ACL: 'public-read'};

                bucket.upload(params, (err, res) => {
                    if (res && res.Location) {
                        resolve(res.Location);
                    } else {
                        reject(null);
                    }
                });
            } else {
                reject(null);
            }
        });


        p.then(url => {
            const dataToSend = {
                title: data.title,
                tehsilName: data.tehsilName,
                area: data.area,
                image: url
            };
            return this.api.postPondData(dataToSend).toPromise();
        }).then(url => {
            this.dataFromService.area = '';
            this.dataFromService.title = '';
            this.dataFromService.tehsilName = '';
        });


        // var dataToSend = {
        //     title: data.title,
        //     tehsilName: data.tehsilName,
        //     area: data.area
        // };
    }

}
