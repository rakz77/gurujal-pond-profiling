import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ApiService} from './../api.service';
import {ModalController} from '@ionic/angular';
import {HttpClient} from '@angular/common/http';
import {CreatePondModalPage} from '../create-pond-modal/create-pond-modal.page';
import {PondDetailsPage} from '../pond-details/pond-details.page';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-folder',
    templateUrl: './folder.page.html',
    styleUrls: ['./folder.page.scss'],
})

export class FolderPage implements OnInit {
    file;
    ishidden = true;
    downloadURL: Observable<string>;
    ref: AngularFireStorageReference;
    task: AngularFireUploadTask;
    uploadProgress: Observable<number>;

    public folder: string;
    pondData: any;
    pondDataByBlock: any;
    pondImage: any;

    selectedFile: File = null;
    image = null;
    url1 = '';

    pond: {
        id?:string;
        title?: string;
        imageA?: string;
        imageGps?: string
      } = {
        id:"",
        title: "",
        imageA: "",
        imageGps: ""
      };

    constructor(private activatedRoute: ActivatedRoute, 
        public api: ApiService, 
        private modalContoller: ModalController,
        private iab:InAppBrowser,
        private http:HttpClient,
        private storage: AngularFireStorage) {
    }


    ngOnInit() {
        this.folder = this.activatedRoute.snapshot.paramMap.get('id');
        this.getPond();
    }

    openModal() {
        this.modalContoller.create({component: CreatePondModalPage}).then((modalElement) => {
            modalElement.present();
        });
    }

    openDetailsModal(request) {
        let req = request;
        this.modalContoller.create({
            component: PondDetailsPage,
            componentProps: {
                title: req.title,
                area: req.area,
                latitude: req.latitude,
                longitude: req.longitude,
                uniqueId: req.uniqueId,
                capacity: req.capacity,
                ownership: req.ownership,
                tehsilName: req.tehsilName,
                villageName: req.villageName,
                image: req.image
            }
        }).then((modalElement) => {
            modalElement.present();

        });
    }

    async getPond() {
        await this.api.getPondData()
            .subscribe(res => {
                // console.log(res.items);
                this.pondData = res.items;
                // console.log(this.pondData);
            }, err => {
                // console.log(err);
            });
    }

    async getPondDataByBlock(request) {
        await this.api.getPondDataByBlock(request)
            .subscribe(res => {
                // console.log(res.items);
                this.pondDataByBlock = res.items;
                // console.log(this.pondDataByBlock);
            }, err => {
                // console.log(err);
            });
    }

    async updatePondImage(data, loc) {
        const dataToSend = {
            imageA: loc,
            _id: data._id,
            title: data.title
        }
        this.api.putPondImage(dataToSend)
        .subscribe(res => {
            this.onChange(data.title);
            this.uploadProgress = null;
            this.downloadURL = null;
        }, err => {
            // console.log(err);
        });
    }

    uploadFile(event) {
        this.file = event.target.files[0];
        if (this.file) {
          const filePath = Math.random().toString(13).substring(2) + this.file.name;
          const fileRef = this.storage.ref(filePath);
          const task = this.storage.upload(filePath, this.file);
    
          // observe percentage changes
          this.uploadProgress = task.percentageChanges();
          // get notified when the download URL is available
          task.snapshotChanges()
            .pipe(
              finalize(() => {
                this.downloadURL = fileRef.getDownloadURL(); // {{ downloadURL | async }}
                this.downloadURL.subscribe(url => {
                    this.url1 = url;
                });
              })
            )
            .subscribe();
        } else {
          console.log('Ooppsss');
        }
      }

      async onChange(request){
        await this.api.getPondImageByTitle(request)
        .subscribe(res => {
            this.pondImage = res.items[0];
            this.image = this.pondImage.imageA;
            this.ishidden = false;
        }, err => { 
            // console.log(err);
        });
      }
}
