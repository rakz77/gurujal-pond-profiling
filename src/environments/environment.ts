// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCQ6nPwfoo8szzVv6kJok08KPczMu8_Z_w",
    authDomain: "gurujal-b609e.firebaseapp.com",
    databaseURL: "https://gurujal-b609e.firebaseio.com",
    projectId: "gurujal-b609e",
    storageBucket: "gurujal-b609e.appspot.com",
    messagingSenderId: "829116021703",
    appId: "1:829116021703:web:06e4d39b63f443176137e2",
    measurementId: "G-HFHR5E9C13"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
